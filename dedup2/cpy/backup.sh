#!/bin/bash

proj="./proj";
arch="./arch";

{
	proj=$(realpath ${proj}); mkdir -p "$proj/00000000"; echo "Project Folder: $proj";
	arch=$(realpath ${arch}); mkdir -p "$arch"; echo "Archive Folder: $arch";

	for dir in ${proj}/*/ ; do
		dir=$(basename "${dir}");
		echo "Updating: $dir" 1>&2;
		pPath="${proj}/${dir}";
		aPath="${arch}/${dir}";
		[[ -d "$aPath" ]] || {
			echo "  - linking project" 1>&2;
			mkdir -p "$aPath";
		}

		pushd "$pPath";
		changes=$(ls ${pPath} | wc -l);
		(( $changes > 0 )) && {
			echo "  - synchronizing changes" 1>&2;
			zip -r0 "${aPath}/$(date -Iseconds).zip" *;
		};

		popd;

	done;
} 1>/dev/null;
