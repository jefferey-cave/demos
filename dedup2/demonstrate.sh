#!/bin/bash

{
	echo "Rev,Date,Style,Size" > results.csv;
	dstlist=`find . -mindepth 1 -maxdepth 1 -type d -not -path "./bin"`;
	for type in $dstlist; do
		type=`basename $type`;
		echo "Clearing ${type}" 1>&2;
		pushd $type;
			rm -rf proj;
			rm -rf arch;
		popd;
	done;

	datasource="https://github.com/factbook/country-profiles.git";
	datasource="https://github.com/factbook/factbook.json.git";
	echo "Acquiring ${datasource}" 1>&2;
	mkdir -p bin;
	pushd bin;
		repo=$PWD;
		git clone $datasource .;
		git checkout HEAD;
		srclist=$(git --no-pager log --grep="auto-update" --pretty=format:"%aI %H %s" | sort | cut -d ' ' -f 2);
		echo "$srclist" | sed 's/^/ - /' 1>&2;
	popd;

	rev=0;
	for hash in $srclist ; do
		rev=$(($rev+1));
		echo "Retrieving hash ($rev): ${hash}" 1>&2;
		pushd $repo;
			git checkout $hash -q;
			datetime=$(git show -s --format=%cI);
		popd;
		for type in $dstlist; do
			type=`basename $type`;
			echo " - ${type}" 1>&2;
			pushd $type;
				mkdir -p proj/00000000;
				pushd proj/00000000;
					rm -rf ./*;
					cp -a $repo/* ./;
				popd;
				./backup.sh 2>&1 | sed 's/^/   | /' 1>&2;
				size=$(du -s . | cut -f 1);
			popd;

			echo "${rev},${datetime},${type},${size}" >> results.csv;
		done;
		echo " - done." 1>&2;
	done;
} 1>/dev/null;
