#!/bin/bash

proj="./proj";
arch="./arch";

function SetupFolder {
	f=$1;
	pPath="${proj}/${f}";
	aPath="${arch}/${f}";

	mkdir -p "$aPath";
	git init "$pPath" --separate-git-dir="$aPath";
}

{
	proj=$(realpath ${proj}); mkdir -p "$proj/00000000"; echo "Project Folder: $proj";
	arch=$(realpath ${arch}); mkdir -p "$arch"; echo "Archive Folder: $arch";

	for dir in ${proj}/*/ ; do
		dir=$(basename "${dir}");
		echo "Updating: $dir" 1>&2;
		pPath="${proj}/${dir}";
		[[ -f "$pPath/.git" ]] || {
			echo "  - linking project" 1>&2;
			SetupFolder "$dir";
		}

		pushd "$pPath";
		changes=$(git status -s | wc -l);
		(( $changes > 0 )) && {
			echo "  - synchronizing changes" 1>&2;
			git add *;
			git commit . -m "$(date -Iminutes)"
		};
		popd;

	done;
} 1>/dev/null;
