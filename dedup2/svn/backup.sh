#!/bin/bash

proj="./proj";
arch="./arch";

function SetupFolder {
	f=$1;
	pPath="${proj}/${f}";
	aPath="${arch}/${f}";
	tPath=$(mktemp -d);

	mkdir -p "$aPath";
	svnadmin create "$aPath" && echo "Repository created." || echo "Repository already exists ($?)";
	svn checkout "file://${aPath}" "$tPath";
	mv "$tPath/.svn" "$pPath/.svn";
	rm -rf "$tPath";
}

{
	proj=$(realpath ${proj}); mkdir -p "$proj/00000000"; echo "Project Folder: $proj";
	arch=$(realpath ${arch}); mkdir -p "$arch"; echo "Archive Folder: $arch";

	for dir in ${proj}/*/ ; do
		dir=$(basename "${dir}");
		echo "Updating: $dir" 1>&2;
		pPath="${proj}/${dir}";
		[[ -d "$pPath/.svn" ]] || {
			echo "  - linking project" 1>&2;
			SetupFolder "$dir";
		}

		pushd "$pPath";
		(( $(svn status | wc -l) > 0 )) && {
			echo "  - synchronizing changes" 1>&2;
			svn status | grep -e "^\?" | cut -c 9- | xargs -I {} svn add {};
			svn status | grep -e "^\!" | cut -c 9- | xargs -I {} svn del {};
			svn commit . -m "$(date -Iminutes)";
			svn cleanup;
		};
		popd;

	done;
} 1>/dev/null;
